import { Component, OnInit } from '@angular/core';

import { CourseService } from './course.service';
import { Course } from '../common/interfaces/course.interface';
import { Topic } from '../common/interfaces/topic.interface';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddCoursePage } from '../add-course/add-course.page';

@Component({
  selector: 'app-course',
  templateUrl: './course.page.html',
  styleUrls: ['./course.page.scss'],
})
export class CoursePage implements OnInit {
  courseList: Array<any>;
  selectedCourse: any;
  upcomingTopic: string;
  completedTopics: Array<Topic>;
  remaningTopics: Array<Topic>;
  currentTopic: Topic;
  progressValue: number;
  flag: boolean;
  totalTopicCount: number;
  reamaningTopicCount: number;
  completedTopicCount: number;
  segment: any;
  constructor(
    private courseService: CourseService,
    private route: ActivatedRoute,
    private modalController: ModalController,
  ) {}

  ngOnInit() {
    this.currentTopic = {} as Topic;
    this.segment = 0;
    this.courseList = [];
    this.courseList = [];
    this.selectedCourse = { program: '', course_name: '' } as any;
    this.remaningTopics = [];
    this.completedTopics = [];
    this.progressValue = 0;
    this.totalTopicCount = 0;
    this.reamaningTopicCount = 0;
    this.completedTopicCount = 0;

    this.flag = false;

    if (this.route.snapshot.params.course) {
      this.selectedCourse.program = this.route.snapshot.params.program;
      this.selectedCourse.course_name = this.route.snapshot.params.course;
      this.getCourse();
    } else {
      if (this.courseService.selectedCourse.value.program !== '') {
        this.selectedCourse.program = this.courseService.selectedCourse.value.program;
        this.selectedCourse.course_name = this.courseService.selectedCourse.value.course_name;
        this.getCourse();
      }
    }
    this.getCourseList();
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1.program == o2.program && o1.course_name == o2.course_name;
  }

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;

        if (this.courseService.selectedCourse.value.program === '') {
          this.selectedCourse =
            this.courseList && this.courseList.length
              ? this.courseList[0]
              : { program: '', course_name: '' };
          this.getCourse();
        }
      },
      error: error => {},
    });
  }

  getCourse() {
    this.courseService.selectedCourse.next({
      program: this.selectedCourse.program,
      course_name: this.selectedCourse.course_name,
    });
    this.courseService
      .getCourse(this.selectedCourse.program, this.selectedCourse.course_name)
      .subscribe({
        next: res => {
          this.selectedCourse.topic = res[0].topics;

          this.remaningTopics = [];
          this.completedTopics = [];
          for (const topic of res[0].topics) {
            if (
              topic.status === 'Remaining' ||
              topic.status === 'In Progress'
            ) {
              this.remaningTopics.push(topic);
            } else {
              this.completedTopics.push(topic);
            }
          }
          if (this.remaningTopics.length !== 0)
            this.remaningTopics.forEach((remaningTopic, i) => {
              if (remaningTopic.status === 'In Progress') {
                this.currentTopic = this.remaningTopics.splice(i, 1)[0];

                this.flag = true;
              }
            });
          else this.currentTopic = {} as Topic;

          this.totalTopic(res[0]);
          this.getCourseProgression(res[0]);
        },
      });
  }

  startTopic(index: number) {
    this.currentTopic = this.remaningTopics.splice(index, 1)[0];

    this.courseService
      .changeTopicStatus(
        this.selectedCourse.program,
        this.selectedCourse.course_name,

        this.currentTopic.topic_name,
      )
      .subscribe({
        next: res => {
          this.flag = true;
          this.getCourse();
        },
      });
  }

  completeTopic() {
    this.flag = false;
    if (this.currentTopic.topic_name) {
      this.courseService
        .changeTopicStatus(
          this.selectedCourse.program,
          this.selectedCourse.course_name,

          this.currentTopic.topic_name,
        )
        .subscribe({
          next: res => {
            this.currentTopic = {} as Topic;
            this.getCourse();
          },
        });
    }
  }

  getCourseProgression(course: Course) {
    this.progressValue = course.topics.length;
    this.progressValue =
      (this.completedTopics.length / this.progressValue) * 100;
    this.progressValue = this.progressValue / 100;
  }

  async addTopic() {
    const modal = await this.modalController.create({
      component: AddCoursePage,
      cssClass: 'addCourse-modal-page',
      componentProps: {
        selectedCourse: this.selectedCourse,
      },
    });

    await modal.present();
    modal.onDidDismiss().then(success => {
      this.getCourse();
    });
  }

  totalTopic(course: Course) {
    this.totalTopicCount = course.topics.length;
    this.reamaningTopicCount = this.remaningTopics.length;
    this.completedTopicCount = this.completedTopics.length;
  }
}
