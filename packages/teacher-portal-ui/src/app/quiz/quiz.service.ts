import { Injectable } from '@angular/core';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  BEARER_TOKEN_PREFIX,
} from '../constants/storage';
import { switchMap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StorageService } from '../api/storage/storage.service';
import { from } from 'rxjs/internal/observable/from';
import { QUIZGET_ENDPOINT } from '../constants/url-strings';
import { APIResponse } from '../common/interfaces/note.interface';

@Injectable({
  providedIn: 'root',
})
export class QuizService {
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getQuiz(program_name: string, course_name: string, topic_name: string) {
    const params = new HttpParams()
      .set('program_name', program_name)
      .set('course_name', course_name)
      .set('topic_name', topic_name);

    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      switchMap(token => {
        const headers = {
          [AUTHORIZATION]: BEARER_TOKEN_PREFIX + token,
        };
        return this.http.get<APIResponse>(QUIZGET_ENDPOINT, {
          params,
          headers,
        });
      }),
    );
  }
}
