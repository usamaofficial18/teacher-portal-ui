import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuizPage } from './quiz.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CourseService } from '../course/course.service';

import { of, BehaviorSubject } from 'rxjs';
import { QuizService } from './quiz.service';

describe('QuizPage', () => {
  let component: QuizPage;
  let fixture: ComponentFixture<QuizPage>;
  const selectedCourse = new BehaviorSubject<string>('');
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuizPage],

      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      providers: [
        {
          provide: CourseService,
          useValue: {
            getCourse: (...args) => of([{ topics: [] }]),
            selectedCourse,
            getCourseList: (...args) => of([{}]),
          },
        },
        {
          provide: QuizService,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(QuizPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
