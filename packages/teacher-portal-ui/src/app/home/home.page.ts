import { Component, OnInit } from '@angular/core';
import { ACCESS_TOKEN, LOGGED_IN } from '../constants/storage';
import { ChartType } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { CourseService } from '../course/course.service';
import { Course } from '../common/interfaces/course.interface';
import { StorageService } from '../api/storage/storage.service';
import { LoginService } from '../api/login/login.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  loggedIn: boolean;
  picture: string;
  expires: string;
  accessToken: string;
  courseList: Array<Course>;
  fullName: string;

  constructor(
    private courseService: CourseService,
    private readonly storage: StorageService,
    private readonly login: LoginService,
    private readonly appService: AppService,
  ) {}

  ngOnInit() {
    this.storage.getItem(ACCESS_TOKEN).then(token => {
      this.loggedIn = token ? true : false;
    });
    this.courseList = [];
    this.setUserSession();
    this.getCourseList();
    this.loadProfile();
  }

  setUserSession() {
    this.login.changes.subscribe({
      next: event => {
        if (event.key === LOGGED_IN && event.value === false) {
          this.loggedIn = false;
        }
      },
      error: error => {},
    });
  }

  doughnutChartLabels: Label[] = ['Completed', 'Remaining'];

  doughnutChartType: ChartType = 'doughnut';

  public doughnutChartColors: Color[] = [
    {
      backgroundColor: ['#3C43D9', '#D1D3F6'],
      borderWidth: 0,
    },
  ];

  public memberChartOptions: any = {
    responsive: true,
    legend: {
      display: false,
      labels: {
        display: false,
      },
    },
  };

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;
        this.donutChartDisplay();
      },
    });
  }

  donutChartDisplay() {
    let count: number = 0;
    this.courseList.forEach(course => {
      count = course.topics.length;
      course.remaining = 0;
      course.topics.forEach(topic => {
        if (topic.status === 'Remaining') {
          course.remaining++;
        }
      });
      course.remaining = (course.remaining / count) * 100;
      course.completed = 100 - course.remaining;
    });
  }

  loadProfile() {
    this.appService.loadProfile().subscribe({
      error: error => {
        this.loggedIn = false;
        this.appService.setupImplicitFlow();
      },
      next: profile => {
        this.loggedIn = true;
        this.fullName = profile.name;
      },
    });
  }
}
