export const DIRECT_PROFILE_ENDPOINT = '/api/direct/v1/profile';
export const COURSELIST_ENDPOINT = '/api/course/v1/list';
export const COURSE_ENDPOINT = '/api/course/v1/get';
export const TOPIC_STATUS_ENDPOINT = '/api/topic/v1/update_status';
export const ADD_TOPIC_ENDPOINT = '/api/topic/v1/add_topic/';
export const QUIZGET_ENDPOINT = '/api/quiz/v1/get';
export const ARTICLElIST_ENDPOINT = '/api/article/v1/list';
