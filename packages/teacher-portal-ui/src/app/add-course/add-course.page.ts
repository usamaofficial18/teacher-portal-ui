import { Component, OnInit, Input } from '@angular/core';
import { CourseService } from '../course/course.service';
import { ModalController } from '@ionic/angular';
import { Topic } from '../common/interfaces/topic.interface';
import { Course } from '../common/interfaces/course.interface';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.page.html',
  styleUrls: ['./add-course.page.scss'],
})
export class AddCoursePage implements OnInit {
  newTopic: Topic;
  @Input() selectedCourse: Course;
  disableInput: boolean;

  constructor(
    private courseService: CourseService,
    private modalController: ModalController,
  ) {}

  ngOnInit() {
    this.newTopic = {} as Topic;
    this.newTopic.topic_name = '';
    this.newTopic.date = '';
    this.disableInput = true;
  }

  addInput() {
    if (this.newTopic.topic_name === '') {
      this.disableInput = true;
    } else this.disableInput = false;
  }

  addTopic() {
    this.courseService
      .addTopic(
        this.newTopic.topic_name,
        this.selectedCourse.program,
        this.selectedCourse.course_name,
      )
      .subscribe({
        next: res => {
          this.modalController.dismiss();
        },
      });
  }
}
