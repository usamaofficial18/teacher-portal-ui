import { Component, OnInit, Input } from '@angular/core';
import { Note } from '../common/interfaces/note.interface';
import { Course } from '../common/interfaces/course.interface';
import { ImportantNotesService } from '../important-note/important-notes.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-draft',
  templateUrl: './add-draft.page.html',
  styleUrls: ['./add-draft.page.scss'],
})
export class AddDraftPage implements OnInit {
  newDraft: Note;
  @Input() selectedCourse: Course;
  constructor(
    private importantService: ImportantNotesService,
    private modalController: ModalController,
  ) {}

  ngOnInit() {
    this.newDraft = {} as Note;
    this.newDraft.title = '';
    this.newDraft.description = '';
  }
  addDraft() {
    this.importantService.addDraft(
      this.newDraft,
      this.selectedCourse.program,
      this.selectedCourse.course_name,
    );
    this.modalController.dismiss();
  }
  cancelDraft() {
    this.modalController.dismiss();
  }
}
