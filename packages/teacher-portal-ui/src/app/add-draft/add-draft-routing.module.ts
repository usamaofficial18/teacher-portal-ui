import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddDraftPage } from './add-draft.page';

const routes: Routes = [
  {
    path: '',
    component: AddDraftPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddDraftPageRoutingModule {}
