import { Question } from './quiz.interface';

export interface Topic {
  topic_name: string;
  date?: string;
  status?: string;
  questionList: Array<Question>;
}
