import { Injectable } from '@angular/core';
import { Attendance } from '../common/interfaces/attendance.interface';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AttendanceService {
  attendanceList: Array<Attendance>;

  constructor() {
    this.attendanceList = [
      {
        program: 'BSc it',
        course_name: 'programming in  c and c',
        attendancesList: [
          {
            date: '31/12/2019',
            rollno: '20',
            name: 'Usama',
            remarks: 'Absent',
          },
          {
            date: '19/12/2019',
            rollno: '21',
            name: 'raj',
            remarks: 'Absent',
          },
          {
            date: '19/12/2019',
            rollno: '23',
            name: 'sudhakar',
            remarks: 'Absent',
          },
        ],
      },
      {
        program: 'BSc it',
        course_name: 'IONIC',
        attendancesList: [
          {
            date: '19/12/2019',
            rollno: '20',
            name: 'Usama',
            remarks: 'Absent',
          },
          {
            date: '19/12/2019',
            rollno: '21',
            name: 'raj',
            remarks: 'Present',
          },
          {
            date: '30/12/2019',
            rollno: '23',
            name: 'sudhakar',
            remarks: 'Present',
          },
        ],
      },
      {
        program: 'std xth',
        course_name: 'physics',
        attendancesList: [
          {
            date: '31/12/2019',
            rollno: '20',
            name: 'samiksha',
            remarks: 'Present',
          },
          {
            date: '31/12/2019',
            rollno: '21',
            name: 'raj',
            remarks: 'Absent',
          },
          {
            date: '20/12/2019',
            rollno: '23',
            name: 'sudhakar',
            remarks: 'Present',
          },
        ],
      },
    ];
  }
  getAttendance(program: string, course_name: string) {
    let foundCourse = {} as Attendance;

    for (const course of this.attendanceList) {
      if (course.course_name === course_name && course.program === program) {
        foundCourse = course;
      }
    }

    return of(foundCourse);
  }
}
