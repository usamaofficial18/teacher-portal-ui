import { Component, OnInit } from '@angular/core';

import { ImportantNotesService } from './important-notes.service';
import { Course } from '../common/interfaces/course.interface';
import { CourseService } from '../course/course.service';
import { ModalController } from '@ionic/angular';
import { AddDraftPage } from '../add-draft/add-draft.page';
import { Topic } from '../common/interfaces/topic.interface';

@Component({
  selector: 'app-important-note',
  templateUrl: './important-note.page.html',
  styleUrls: ['./important-note.page.scss'],
})
export class ImportantNotePage implements OnInit {
  selectedCourse: any;
  noteList: Array<Course>;
  courseList: Array<Course>;
  topics: Array<Topic>;
  selectedTopic: Topic;
  search: any;

  constructor(
    private importantService: ImportantNotesService,
    private courseService: CourseService,
    private modalController: ModalController,
  ) {
    this.courseList = [];
    this.noteList = [];
    this.selectedCourse = { program: '', course_name: '' } as any;
    this.selectedCourse.notes = [];
    this.selectedTopic = {} as Topic;
    this.selectedTopic.topic_name = '';
    this.topics = [];
    this.search = '';
  }

  ngOnInit() {
    if (this.courseService.selectedCourse.value.program !== '') {
      this.selectedCourse.program = this.courseService.selectedCourse.value.program;
      this.selectedCourse.course_name = this.courseService.selectedCourse.value.course_name;
    }
    this.getCourseList();
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1.program == o2.program && o1.course_name == o2.course_name;
  }

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;
        if (this.courseService.selectedCourse.value.program === '') {
          this.selectedCourse =
            this.courseList && this.courseList.length
              ? this.courseList[0]
              : { program: '', course_name: '' };
        }
        this.getCourse();
      },
    });
  }

  getCourse() {
    this.courseService.selectedCourse.next({
      program: this.selectedCourse.program,
      course_name: this.selectedCourse.course_name,
    });

    this.courseService
      .getCourse(this.selectedCourse.program, this.selectedCourse.course_name)
      .subscribe({
        next: res => {
          this.topics = res[0].topics;
          this.getNote();
        },
      });
  }

  getNote() {
    this.noteList = [];
    this.importantService
      .getNote(
        this.selectedCourse.program,
        this.selectedCourse.course_name,
        this.selectedTopic.topic_name,
        this.search,
      )
      .subscribe({
        next: res => {
          this.noteList = res.docs;
        },
        error: error => {},
      });
  }

  async newDraft() {
    const modal = await this.modalController.create({
      component: AddDraftPage,

      cssClass: 'addCourse-modal-page',
      componentProps: {
        selectedCourse: this.selectedCourse,
      },
    });

    await modal.present();
    modal.onDidDismiss().then(success => {
      this.getNote();
    });
  }
}
